package afd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nodos.Nodo;
import nodos.Par;

import java.util.Queue;
import java.util.Set;

import tabla.TablaImprimir;

public class AFD {
	private Par par;
	private String[] letra;
	private TablaImprimir tabla;
	private Map<Set<Integer>, Integer> mapa;
	private Set<Integer> conjuntoAux;
	private Queue<Integer> cola = new LinkedList<>();
	private List<Character[]> afd = new ArrayList<>();
	private List<Character> estadoFinal = new ArrayList<>();
	
	private int estado = 'A';

	public AFD(Par par, String[] letra) {
		this.par = par;
		this.letra = letra;
		tabla = new TablaImprimir(letra.length - 1, true);
		tabla.agregarFila();
		for (int i = 0; i < letra.length - 1; i++) {
			tabla.agregarColumna(letra[i]);
		}
		mapa = new HashMap<>();
	}

	public List<Character[]> obtenerAFD() {
		List<Character[]> regularAFD = new ArrayList<>();
		for (Character[] caract : afd) {
			if(obtenerConjunto(caract[0])==null||obtenerConjunto(caract[0]).isEmpty()) {
				continue;
			}
			else {
				Character[] nuevoCaracter = new Character[caract.length];
				for(int i=0;i<caract.length;i++) {
					if(caract[i]==null)
						continue;
					Set<Integer> conjunto = obtenerConjunto(caract[i]);
					if(conjunto == null||conjunto.isEmpty())
						nuevoCaracter[i] = null;
					else
						nuevoCaracter[i] = caract[i];
				}
				regularAFD.add(nuevoCaracter);
			}
		}
		return regularAFD;
	}
	
	public List<Character> obtenerEstadoFinal() {
		return estadoFinal;
	}
	
	public String[] obtenerLetra() {
		return letra;
	}
	
	public void imprimirAFD() {
		System.out.println();
		System.out.println("--------AFD--------");
		System.out.print(tabla);
		for (Entry<Set<Integer>, Integer> entrada : mapa.entrySet()) { 
			if(entrada.getValue()==-1)
				continue;
			System.out.println((char)entrada.getValue().intValue() +" = " + entrada.getKey() + (esInicio(entrada.getKey())?" START ":"") + (esFinal(entrada.getKey())?" END ":"") ); 
		}
		System.out.println("--------AFD--------");
	}

	private boolean esInicio(Set<Integer> conjunto) {
		for (Integer numero : conjunto) {
			if(numero == par.nodoInicio.getEstado())
				return true;
		}
		return false;
	}
	
	private boolean esFinal(Set<Integer> conjunto) {
		for (Integer numero : conjunto) {
			if(numero == par.nodoFin.getEstado()) {
				estadoFinal.add(new Character((char)getCaracter(conjunto).intValue()));	
				return true;
			}
		}
		return false;
	}
	
	public void crearAFD() {
		conjuntoAux = new HashSet<>();
		Set<Integer> inicio= mover(par.nodoInicio,-1);
		mapa.put(inicio, estado);
		cola.add(estado++);
		while(!cola.isEmpty()) {
			Character[] lineaDFA = new Character[letra.length-1];
			int carac = cola.poll();
			tabla.agregarFila();
			tabla.agregarColumna((char)carac);
			lineaDFA[0] = (char)carac;
			Set<Integer> conjunto = obtenerConjunto(carac);
			for(int i=1;i<letra.length-1;i++) {
				conjuntoAux = new HashSet<>();
				Set<Integer> conjuntoMedio = new HashSet<>();
				for (Integer numero : conjunto) {
					Nodo nodo = getNodo(par.nodoInicio, numero);
					revisitar();
					if(nodo==null) {
						continue;
					}
					else if((char)nodo.getArista() == letra[i].charAt(0)) {
						conjuntoMedio.add(nodo.siguiente.getEstado());
					}
				}
				for (Integer numero : conjuntoMedio) {
					Nodo nodo = getNodo(par.nodoInicio, numero);
					revisitar();
					mover(nodo, -1);
				}
				Integer c = getCaracter(conjuntoAux);
				if(c==null) {
					if(conjuntoAux.isEmpty()) {
						mapa.put(conjuntoAux, -1);
						tabla.agregarColumna("null");
						lineaDFA[i] = null;
					}
					else {
						cola.add(estado);
						tabla.agregarColumna((char)estado);
						lineaDFA[i] = (char)estado;
						mapa.put(conjuntoAux, estado++);
					}
				}
				else {
					if(c==-1) {
						tabla.agregarColumna("null");
						lineaDFA[i] = null;
					}
					else {
						lineaDFA[i] = (char)c.intValue();
						tabla.agregarColumna((char)c.intValue());
					}
				}
			}
			afd.add(lineaDFA);
		}
	}

	private Set<Integer> mover(Nodo startNode, int i) {
		conectar(startNode, i);
		revisitar();
		return conjuntoAux;
	}

	private void conectar(Nodo nodo, int i) {
		if(nodo==null||nodo.fueVisitado())
			return;
		nodo.setVisitado();
		conjuntoAux.add(nodo.getEstado());
		if(nodo.getArista()==-1||nodo.getArista()==i) {
			conectar(nodo.siguiente, i);
			conectar(nodo.otroSiguiente, i);
		}
		else
			return;
	}
	
	private Nodo getNodo(Nodo nodo, int estadoInicio) {
		if (nodo == null || nodo.fueVisitado())
			return null;
		nodo.setVisitado();
		if (nodo.getEstado() == estadoInicio)
			return nodo;
		if (nodo.getEstado() > estadoInicio)
			return null;
		Nodo aux1 = getNodo(nodo.siguiente, estadoInicio);
		Nodo aux2 = getNodo(nodo.otroSiguiente, estadoInicio);
		if (aux1 != null)
			return aux1;
		if (aux2 != null)
			return aux2;
		return null;
	}
	
	private Integer getCaracter(Set<Integer> conjunto) {
		return mapa.get(conjunto);
	}
	
	private Set<Integer> obtenerConjunto(int character) {
		for (Entry<Set<Integer>, Integer> m :mapa.entrySet())  {
			if(m.getValue()==character)
				return m.getKey();
		}
		return null;
	}
	
	private void revisitar(Nodo nodo) {
		if (nodo == null || !nodo.fueVisitado()) {
			return;
		}
		nodo.setNoVisitado();
		revisitar(nodo.siguiente);
		revisitar(nodo.otroSiguiente);
	}
	private void revisitar() {
		par.nodoInicio.setNoVisitado();
		revisitar(par.nodoInicio.siguiente);
		revisitar(par.nodoInicio.otroSiguiente);
	}
	
	@Override
	public String toString() {
		return conjuntoAux.toString();
	}
}
