package afdMinimo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import tabla.TablaImprimir;

public class AFDMinimo {
	private List<Character[]> afd;
	private List<Character[]> afdMinimo = new ArrayList<>();
	private String[] letra;
	private List<Character> estadoFinal;
	private TablaImprimir tabla;
	private Set<Set<Character>> conjuntoTotal = new HashSet<>();
	private static boolean correcto = false;
	private ArrayList<String> mensaje = new ArrayList<>();

	private Map<Character, Character> mapa = new HashMap<>();
	
	public AFDMinimo(List<Character[]> afd, List<Character> estadoFinal, String[] letra) {
		this.afd = afd;
		this.estadoFinal = estadoFinal;
		this.letra = letra;
		tabla = new TablaImprimir(letra.length - 1, true);
		tabla.agregarFila();
		for (int i = 0; i < letra.length - 1; i++) {
			tabla.agregarColumna(letra[i]);
		}
	}


	public void minimizar() {
		iniciar(conjuntoTotal);
		int contador = 0;
		while (true) {
			if (contador == conjuntoTotal.size())
				break;
			else
				contador = 0;
			Set<Set<Character>> copiaConjuntoTotal = new HashSet<>(conjuntoTotal);
			for (Set<Character> conjunto : copiaConjuntoTotal) {
				if (esIndivisible(conjunto)) {
					contador++;
					continue;
				} else {
					minimizar(conjunto);
				}
			}
		}
	}

	private void minimizar(Set<Character> estado) {
		conjuntoTotal.remove(estado);
		Map <String,String> mapa = new HashMap<>();
		for (Character character : estado) {
			String cadena ="";
			for (int i = 1; i < letra.length-1; i++) {
					cadena += mover(character, letra[i].charAt(0))+"";
			}
			String conjuntoAux = mapa.get(cadena);
			if(conjuntoAux==null) {
				mapa.put(cadena, character+"");
			}
			else {
				conjuntoAux +=character;
				mapa.put(cadena, conjuntoAux);
			}
		}
		for(String cad : mapa.values()) {
			Set<Character> conjunto = new HashSet<>();
			for(int i=0;i<cad.length();i++)
				conjunto.add(cad.charAt(i));
			conjuntoTotal.add(conjunto);
		}
	}

	private boolean enConjuntoTotal(Set<Character> aux) {
		if(aux.isEmpty())
			return true;
		Set<Integer> pos = new HashSet<>();
		for (Character character : aux) {
			pos.add(getConjuntoNumero(character));
		}
		return pos.size()==1;
	}

	private int getConjuntoNumero(Character caract) {
		int i=0;
		for (Set<Character> conjuntos : conjuntoTotal) {
			for (Character elem : conjuntos) {
				if(elem.equals(caract))
					return i;
			}
			i++;
		}
		return -1;
	}

	private void iniciar(Set<Set<Character>> totalSet) {
		Set<Character> terminal = new HashSet<>();
		Set<Character> noTerminal = new HashSet<>();
		for (Character[] caract : afd) {
			if (esEstadoFinal(caract[0]))
				terminal.add(caract[0]);
			else
				noTerminal.add(caract[0]);
		}
		totalSet.add(noTerminal);
		totalSet.add(terminal);
	}

	private boolean esEstadoFinal(Character carac) {
		for (Character estado : estadoFinal) {
			if (estado.equals(carac))
				return true;
		}
		return false;
	}

	private boolean esIndivisible(Set<Character> conjunto) {
		if (conjunto.size() == 1)
			return true;
		else {
			for (int i = 1; i < letra.length - 1; i++) {
				Set<Character> aux = new HashSet<>();
				for (Character carac : conjunto) {
					aux.add(mover(carac, letra[i].charAt(0)));
				}
				if (enConjuntoTotal(aux))
					continue;
				else {
					return false;
				}
			}
		}
		return true;
	}

	public void imprimirAFDMinimo() {
		for (Character[] characters : afdMinimo) {
			tabla.agregarFila();
			for (Character character : characters) {
				tabla.agregarColumna(character);
			}
		}
		System.out.println("--------AFD MINIMO--------");
		System.out.print(tabla);
		System.out.println("estado inicial: [A]");
		System.out.println("estado final: "+estadoFinal);
		System.out.println("--------AFD MINIMO--------");
	}

	public void mezclar(){
		for (Set<Character> carac : conjuntoTotal) {
			if(carac.size()==1)
				continue;
			else {
				int i=0;
				char llave = 0;
				for (Character caracter : carac) {
					if(i++==0)
						llave = caracter;
					else 
						mapa.put(caracter, llave);
				}
			}
		}
		List<Character[]> auxAFDMinimo = new ArrayList<>();
		for (Character[] carac : afd) {
			if(ignorar(carac[0])) {
				estadoFinal.remove(carac[0]);
				continue;
			}
			else {
				Character [] nuevoCarac = new Character[carac.length];
				int i=0;
				for (Character caracAux : carac) {
					if(reemplazar(caracAux))
						nuevoCarac[i] = mapa.get(caracAux);
					else {
						nuevoCarac[i] = caracAux;
					}
						i++;
				}
			auxAFDMinimo.add(nuevoCarac);
			}
		}
		List<Character> estadoFinal = new ArrayList<>();
		for (Character[] auxCarac : auxAFDMinimo) {
			if(estadoFinal.contains(auxCarac[0]))
				continue;
			else {
				estadoFinal.add(auxCarac[0]);
				afdMinimo.add(auxCarac);
			}
		}
		
	}
	private boolean reemplazar(Character carac) {
		Character valor = mapa.get(carac);
		return valor!=null;
	}


	private boolean ignorar(Character carac) {
		for (Entry<Character, Character> mapa :mapa.entrySet())  {
			if(mapa.getKey().equals(carac))
				return true;
		}
		return false;
	}


	
	@SuppressWarnings("unused")
	private void sacar(Character caracter, char estadoNuevo) {
		for (Character[] carac : afd) {
			if(carac[0]==caracter)
				continue;
			else {
				Character[] nuevoCarac = new Character[carac.length];
				int i=0;
				for (Character car : carac) {
					if(car == caracter)
						nuevoCarac[i]=estadoNuevo;
					else
						nuevoCarac[i]=car;
					i++;
				}
				afdMinimo.add(nuevoCarac);
			}
		}
	}

	private Character mover(Character estado, char entrada) {
		for (Character[] carac : afd) {
			if (carac[0] != estado)
				continue;
			else {
				int num = getPos(entrada);
				return carac[num]==null?null:carac[num];
			}
		}
		return null;
	}

	private int getPos(char entrada) {
		for (int i = 1; i < letra.length - 1; i++) {
			if (letra[i].charAt(0) == entrada)
				return i;
		}
		return -1;
	}
	
	private Character mover(char estado, int pos) {
		for (Character[] caracteres : afdMinimo) {
			if (caracteres[0] != estado)
				continue;
			else {
				return caracteres[pos];
			}
		}
		return null;
	}
	
	public void valido(String cadena) {
		Character estadoAlcanz = new Character('A');
		setCorrecto(false);
		for(int i=0;i<cadena.length();i++) {
			int pos = getPos(cadena.charAt(i));
			if(pos == -1) {
				StringBuilder error = new StringBuilder();
				error.append(cadena + " es incorrecto \r\n");
				setCorrecto(false);
				/*mensaje.add(error.toString());
				for(int q=0;q<i;q++)
					error.append(" ");
				error.append("↑"); */
				System.err.println(error.toString());
				return;
			}else {
				estadoAlcanz = mover(estadoAlcanz, pos);
				if(estadoAlcanz==null) {
					StringBuilder error = new StringBuilder();
					error.append(cadena + " es incorrecto \r\n");
					setCorrecto(false);
					mensaje.add(error.toString());
					/*for(int q=0;q<i;q++)
						error.append(" ");
					error.append("↑"); */
					System.err.println(error.toString());
					return;
				}
			}
		}
		if(esEstadoFinal(estadoAlcanz)) {
			System.out.println(cadena+" es correcta");
			mensaje.add(cadena+" es correcta");
			setCorrecto(true);
		}
		else {
			System.err.println(cadena+" es incorrecto \""+cadena.charAt(cadena.length()-1)+"\"(incompleto)");
			setCorrecto(false);
			mensaje.add(cadena+" es incorrecta");
		}
	}
	
	public static boolean esCorrecto() {
		return correcto;
	}
	
	private void setCorrecto(boolean correc) {
		correcto = correc;
	}
	
	public ArrayList<String> getMensaje(){
		return mensaje;
	}
	
	public void limpiarMensaje() {
		mensaje.clear();
	}
	
}
