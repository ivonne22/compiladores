package afn;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import nodos.Nodo;
import nodos.Par;
import tabla.TablaImprimir;

public class AFN {

	private int estado = 0;
	
	private String expRe;
	private String expReUnida;
	private String posFijo;

	private String[] letras;
	private Par par;
	
	private TablaImprimir tabla;

	public AFN(String expRe) {
		this.expRe = expRe;
		expReUnida = null;
		posFijo = null;
		Set<Character> aux = new HashSet<>();
		for(int i=0;i<this.expRe.length();i++){
			if(esLetra(this.expRe.charAt(i))){
				aux.add(this.expRe.charAt(i));
			}
		}
		letras = new String[aux.size()+2];
		Object []objAux = aux.toArray();
		int i=0;
		letras[i] = "";
		for (;i<objAux.length;i++) {
			letras[i+1] = (char)objAux[i]+"";
		}
		letras[i+1] = "EPSILON";
		tabla = new TablaImprimir(letras.length, true);
		tabla.agregarFila();
		for (String cadena : letras) {
			tabla.agregarColumna(cadena);
		}
	}

	public Par getPar() {
		return par;
	}
	
	public String[] getLetra() {
		return letras;
	}

	public void setPar(Par par) {
		this.par = par;
	}

	public String agregarUnion() {
		int longitud = expRe.length();
		if(longitud==1) {
			System.out.println("Uniendo las expresiones" + expRe);
			expReUnida = expRe;
			return expRe;
		}
		int longitudCadena = 0;
		char cadena[] = new char[2 * longitud + 2];
		char primero, segundo = '0';
		for (int i = 0; i < longitud - 1; i++) {
			primero = expRe.charAt(i);
			segundo = expRe.charAt(i + 1);
			cadena[longitudCadena++] = primero;
			if (primero != '(' && primero != '|' && esLetra(segundo)) {
				cadena[longitudCadena++] = '.';
			}
			else if (segundo == '(' && primero != '|' && primero != '(') {
				cadena[longitudCadena++] = '.';
			}
		}
		cadena[longitudCadena++] = segundo;
		String rString = new String(cadena, 0, longitudCadena);
		System.out.println("Uniendo las expresiones" + rString);
		System.out.println();
		expReUnida = rString;
		return rString;
	}

	private boolean esLetra(char carac) {
		{
			if (carac >= 'a' && carac <= 'z' || carac >= 'A' && carac <= 'Z')
				return true;
			return false;
		}
	}

	public String posFijo() {
		expReUnida = expReUnida + "#";

		Stack<Character> pilaCarac = new Stack<>();
		char carac = '#', carac2, oper;
		pilaCarac.push(carac);
		String salida = "";
		int pos = 0;
		carac = expReUnida.charAt(pos++);
		while (!pilaCarac.empty()) {
			if (esLetra(carac)) {
				salida = salida + carac;
				carac = expReUnida.charAt(pos++);
			} else {
				carac2 = pilaCarac.peek();
				if (isp(carac2) < icp(carac)) {
					pilaCarac.push(carac);
					carac = expReUnida.charAt(pos++);
				} else if (isp(carac2) > icp(carac)) {
					oper = pilaCarac.pop();
					salida = salida + oper;
				} else {
					oper = pilaCarac.pop();
					if (oper == '(')
						carac = expReUnida.charAt(pos++);
				}
			}
		}
		System.out.println("postfix:" + salida);
		System.out.println();
		posFijo = salida;
		return salida;
	}

	private int isp(char c) {
		switch (c) {
		case '#':
			return 0;
		case '(':
			return 1;
		case '*':
			return 7;
		case '+':
			return 7;
		case '.':
			return 5;
		case '|':
			return 3;
		case ')':
			return 8;
		}
		return -1;
	}

	private int icp(char c) {
		switch (c) {
		case '#':
			return 0;
		case '(':
			return 8;
		case '*':
			return 6;
		case '+':
			return 6;
		case '.':
			return 4;
		case '|':
			return 2;
		case ')':
			return 1;
		}
		return -1;
	}

	public void expReAFN() {
		par = new Par();
		Par parAux = new Par();
		Par izq, der;
		ConstructorAFN constructor = new ConstructorAFN();
		char carac[] = posFijo.toCharArray();
		Stack<Par> pila = new Stack<>();
		for (char elemCar : carac) {
			switch (elemCar) {
			case '|':
				izq = pila.pop();
				der = pila.pop();
				par = constructor.operacionOR(der, izq);
				pila.push(par);
				break;
			case '*':
				parAux = pila.pop();
				par = constructor.cerraduraKleene(parAux);
				pila.push(par);
				break;
			case '+':
				parAux = pila.pop();
				par = constructor.cerraduraPositiva(parAux);
				pila.push(par);
				break;
			case '.':
				izq = pila.pop();
				der = pila.pop();
				par = constructor.conexion(der, izq);
				pila.push(par);
				break;
			default:
				par = constructor.unionSimple(elemCar);
				pila.push(par);
				break;
			}
		}
	}

	public void imprimir() {
		estadoExpRe(this.par.nodoInicio);
		visitado(this.par.nodoInicio);
		System.out.println("--------AFN--------");
		tabla.agregarFila();
		imprimirAFN(this.par.nodoInicio);
		System.out.print(tabla);
		visitado(this.par.nodoInicio);
		System.out.println("--------AFN--------");
		System.out.println("estado entrada: " + (this.par.nodoInicio.getEstado()));
		System.out.println("estado salida: " + (this.par.nodoFin.getEstado()));
	}

	private void estadoExpRe(Nodo empiezaAFN) {
		if (empiezaAFN == null || empiezaAFN.fueVisitado()) {
			return;
		}
		empiezaAFN.setVisitado();
		empiezaAFN.setEstado(estado++);
		estadoExpRe(empiezaAFN.siguiente);
		estadoExpRe(empiezaAFN.otroSiguiente);
	}
	private void visitado(Nodo empiezaAFN) {
		if (empiezaAFN == null || !empiezaAFN.fueVisitado()) {
			return;
		}
		empiezaAFN.setNoVisitado();
		visitado(empiezaAFN.siguiente);
		visitado(empiezaAFN.otroSiguiente);
	}

	private void imprimirAFN(Nodo empiezaAFN) {
		if (empiezaAFN == null || empiezaAFN.fueVisitado()) {
			return;
		}

		empiezaAFN.setVisitado();

		imprimeNodoAFN(empiezaAFN);
		if (empiezaAFN.siguiente != null) {
			tabla.agregarFila();
		}
		imprimirAFN(empiezaAFN.siguiente);
		imprimirAFN(empiezaAFN.otroSiguiente);
	}

	private void imprimeNodoAFN(Nodo nodo) {
		if (nodo.siguiente != null) {
			tabla.agregarColumna(nodo.getEstado());
			if(nodo.getArista()==-1) {
				for(int i=0;i<letras.length-2;i++) {
					tabla.agregarColumna(" ");
				}
				if (nodo.otroSiguiente != null)
					tabla.agregarColumna("{"+nodo.siguiente.getEstado()+","+nodo.otroSiguiente.getEstado()+"}");
				else
					tabla.agregarColumna("{"+nodo.siguiente.getEstado()+"}");
				}
			else {
				int index = getPos(""+(char)nodo.getArista());
				for(int i=0;i<letras.length-1;i++) {
					if(i!=index)
						tabla.agregarColumna(" ");
					else {
						if (nodo.otroSiguiente != null)
							tabla.agregarColumna("{"+nodo.siguiente.getEstado()+","+nodo.otroSiguiente.getEstado()+"}");
						else
							tabla.agregarColumna("{"+nodo.siguiente.getEstado()+"}");
					}
				}
			}
		}
		else {
			tabla.agregarColumna(nodo.getEstado());
			tabla.agregarColumna(" ");
			tabla.agregarColumna(" ");
			tabla.agregarFila();
		}
	}
	
	//“�?,a,b,EPS
	private int getPos(String carac) {
		for (int i=0;i<letras.length;i++) {
			if(letras[i].equals(carac))
				return i-1;
		}
		return -1;
	}
	
}
