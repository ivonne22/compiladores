package afn;

import nodos.Par;

public class ConstructorAFN {
	private ProcesosAFN procesosAFN = null;

	public ConstructorAFN() {
		procesosAFN = new ProcesosAFN();
	}

	public Par cerraduraKleene(Par parEntrada) {
		Par parSalida = new Par();
		parSalida.nodoInicio = procesosAFN.nuevoAFN();
		parSalida.nodoFin = procesosAFN.nuevoAFN();

		parSalida.nodoInicio.siguiente = parEntrada.nodoInicio;
		parEntrada.nodoFin.siguiente = parSalida.nodoFin;

		parSalida.nodoInicio.otroSiguiente = parSalida.nodoFin;
		parEntrada.nodoFin.otroSiguiente = parEntrada.nodoInicio;

		parEntrada.nodoInicio = parSalida.nodoInicio;
		parEntrada.nodoFin = parSalida.nodoFin;

		return parSalida;
	}

	public Par cerraduraPositiva(Par parEntrada) {
		Par parSalida = new Par();

		parSalida.nodoInicio = procesosAFN.nuevoAFN();
		parSalida.nodoFin = procesosAFN.nuevoAFN();

		parSalida.nodoInicio.siguiente = parEntrada.nodoInicio;
		parEntrada.nodoFin.siguiente = parSalida.nodoFin;

		parEntrada.nodoFin.otroSiguiente = parSalida.nodoInicio;

		parEntrada.nodoInicio = parSalida.nodoInicio;
		parEntrada.nodoFin = parSalida.nodoFin;

		return parSalida;
	}

	public Par unionSimple(char car) {

		Par parSalida = new Par();
		parSalida.nodoInicio = procesosAFN.nuevoAFN();
		parSalida.nodoFin = procesosAFN.nuevoAFN();
		parSalida.nodoInicio.siguiente = parSalida.nodoFin;
		parSalida.nodoInicio.setArista(car);

		return parSalida;
	}

	public Par operacionOR(Par izq, Par der) {
		Par par = new Par();
		par.nodoInicio = procesosAFN.nuevoAFN();
		par.nodoFin = procesosAFN.nuevoAFN();

		par.nodoInicio.siguiente = izq.nodoInicio;
		par.nodoInicio.otroSiguiente = der.nodoInicio;

		izq.nodoFin.siguiente = par.nodoFin;
		der.nodoFin.siguiente = par.nodoFin;

		return par;
	}

	public Par conexion(Par izq, Par der) {
		Par parSalida = new Par();
		parSalida.nodoInicio = izq.nodoInicio;
		parSalida.nodoFin = der.nodoFin;

		izq.nodoFin.siguiente = der.nodoInicio;

		return parSalida;
	}
}
