package afn;

import java.util.Stack;

import nodos.Nodo;

public class ProcesosAFN {
    private final int max = 256; 
    private Nodo[] vectorEstados = null;
    private Stack<Nodo> pilaAFN = null;
    private int sigPos = 0; 
    private int estadosAFN = 0; 
    
    public ProcesosAFN()  {
    	vectorEstados = new Nodo[max];
    	for (int i = 0; i < max; i++) {
    		vectorEstados[i] = new Nodo();
    	}
    	
    	pilaAFN = new Stack<Nodo>();
    	
    }
    
    public Nodo nuevoAFN()  {
    	Nodo afn = null;
    	if (pilaAFN.size() > 0) {
    		afn = pilaAFN.pop();
    	}
    	else {
    		afn = vectorEstados[sigPos];
    		sigPos++;
    	}
    	
    	afn.vaciarEstado();
    	afn.setEstado(estadosAFN++);
    	afn.setArista(Nodo.EPSILON);
    	
    	return afn;
    }
    
    public void eliminarAFN(Nodo afn) {
    	--estadosAFN;
    	afn.vaciarEstado();
    	pilaAFN.push(afn);
    }
    
   
}
