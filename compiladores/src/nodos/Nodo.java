package nodos;

public class Nodo {
	public static final int EPSILON = -1;
	public static final int VACIO = -2; 

	private int arista;

	public int getArista() {
		return arista;
	}

	public void setArista(int tipo) {
		arista = tipo;
	}

	public Nodo siguiente; 
	public Nodo otroSiguiente; 
	private int estado;
	private boolean visitado = false;
	
	public void setVisitado() {
		visitado = true;
	}

	public void setNoVisitado() {
		visitado = false;
	}
	
	public boolean fueVisitado() {
		return visitado;
	}

	public void setEstado(int numeroEstado) {
		estado = numeroEstado;
	}

	public int getEstado() {
		return estado;
	}


	public void vaciarEstado() {
    	siguiente = otroSiguiente = null;
    	estado = -1;		
	}
	
	@Override
	public String toString() {
		return (char)arista+" "+estado+""+fueVisitado();
	}
}
