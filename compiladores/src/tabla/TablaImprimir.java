package tabla;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TablaImprimir {

	private LinkedList<List> filas = new LinkedList<List>();

	private int columna;

	private int[] longitudColumna;

	private static int margen = 2;

	private boolean imprimirCabecera = false;

	public TablaImprimir(int columna, boolean imprimirCabecera) {
		this.imprimirCabecera = imprimirCabecera;
		this.columna = columna;
		this.longitudColumna = new int[columna];
	}

	public void agregarFila() {
		if (!filas.isEmpty()) {
			List aux = filas.getLast();
			if (aux.isEmpty())
				return;
		}
		List fila = new ArrayList(columna);
		filas.add(fila);
	}


	public TablaImprimir agregarColumna(Object valor) {
		if (valor == null) {
			valor = "NULL";
		}
		List fila = filas.get(filas.size() - 1);
		fila.add(valor);
		int len = valor.toString().getBytes().length;
		if (longitudColumna[fila.size() - 1] < len)
			longitudColumna[fila.size() - 1] = len;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder();

		int sumaLongitud = 0;
		for (int longitud : longitudColumna) {
			sumaLongitud += longitud;
		}
		if (imprimirCabecera)
			buffer.append("|").append(imprimirCaracter('=', sumaLongitud + margen * 2 * columna + (columna - 1))).append("|\n");
		else
			buffer.append("|").append(imprimirCaracter('-', sumaLongitud + margen * 2 * columna + (columna - 1))).append("|\n");
		for (int ii = 0; ii < filas.size(); ii++) {
			List row = filas.get(ii);
			for (int i = 0; i < columna; i++) {
				String o = "";
				if (i < row.size())
					o = row.get(i).toString();
				buffer.append('|').append(imprimirCaracter(' ', margen)).append(o);
				buffer.append(imprimirCaracter(' ', longitudColumna[i] - o.getBytes().length + margen));
			}
			buffer.append("|\n");
			if (imprimirCabecera && ii == 0)
				buffer.append("|").append(imprimirCaracter('=', sumaLongitud + margen * 2 * columna + (columna - 1))).append("|\n");
			else
				buffer.append("|").append(imprimirCaracter('-', sumaLongitud + margen * 2 * columna + (columna - 1))).append("|\n");
		}
		return buffer.toString();
	}

	private String imprimirCaracter(char caracter, int longitud) {
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < longitud; i++) {
			buffer.append(caracter);
		}
		return buffer.toString();
	}

}